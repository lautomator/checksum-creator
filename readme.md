# Checksum Creator

This program creates a check digit. The user enters 7 digits (0-9); the program doubles every other digit. If the doubled digit is greater than 9, those two numbers are added together. All the numbers are summed. The sum is divided by 10. If the remainder is 0, then we have a valid check digit.

Error handling needs to be implemented on a greater scale. This program could be more useful if it was portable: arguments could be made on the command line for the length of the digit string.

