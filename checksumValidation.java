/* This program creates a check digit. The user
 * enters 7 digits (0-9); the program
 * doubles every other digit. If the doubled digit
 * is greater than 9, those two numbers are added
 * together. All the numbers are summed. The sum is
 * divided by 10. If the remainder is 0, then we
 * have a valid check digit. 
 * 
 * Author: John Merigliano
 */

import java.util.*;

public class checksumValidation
{

    public static void main(String[] args) // main
    {
        Scanner input = new Scanner(System.in); // create a scanner
        String digits; // string to hold data
        int len = 0; // intitial length of entry
        do
        {
            System.out.print("Enter 7 digits (0-9): ");
            digits = input.next(); // since char is not accpeted as input, string is used
            len = digits.length(); // length of entry
            System.out.println(validate_entry(len)); // report any errors
        }
        while (len != 7); // length must be 7

        /** function call **/
        System.out.println(checksumCalculation(digits)); // calculate the checksum
    }// main ends


    /** Validate input */
    public static String  validate_entry(int len)
    {
        // store the error
        String error;

        // rule out a bad length
        if (len != 7)
        {
            error = "Error |--> Input 7 digits only (0-9)";
        }
        else
        {
            error = "";
        }

        return error;
    }


    /** the function **/
    /* Calculates the checksum by doubling every other digit,
     * adding the numbers together if the doubled digit is greater
     * then 9, summing all of the numbers. */
    public static double checksumCalculation(String digits)
    {
        // data fields:
        char digit; // single character
        int digitConverted; // character to digit
        int digitConvertedDoubled; // double digit
        int sum = 0; // initial value of sum
        int sumFinal = 0; // final value of sum

        for (int i = 0; i < 7; i++)
        {
            // positions 0, 2, and 4 are not doubled
            if (i % 2 == 0)
            {
                digit = digits.charAt(i); // get character
                digitConverted = (int)digit - 48; // convert to integer
                sum = digitConverted; // add to sum
            }
            // positions 1, 3, 5 are doubled
            else
            {
                digit = digits.charAt(i); // get character
                digitConvertedDoubled = ((int)digit - 48) * 2; // convert to int and double
                // the digit double is > 9
                if (digitConvertedDoubled > 9)
                {
                    digitConvertedDoubled = (digitConvertedDoubled / 10)
                        + (digitConvertedDoubled % 10); // split the digits and add
                    sum = digitConvertedDoubled; // add to sum
                }
                else
                {
                    sum = digitConvertedDoubled; // add to sum
                }
            }
            sumFinal += sum; // add to final sum
        }// loop ends
        return sumFinal; // validate the final sum: div by 10 without a remainder
    }// checksum function ends
}// class ends
